# SécuritéObjetTP2 par LEU Anthony

## Dépendances

- Python 3.10

## Utilisation

Rentrer la commande : python GPS_export.py "nom_du_fichier"
On obtient un fichier "nom_du_fichier".csv contenant les données GPS au format latitude;longitude
