import sys
import csv

coord = []

def parse_GPS(file_name):
    with open(file_name,"rb") as file :
        with open(file_name+".csv","w") as coord:
            data = file.read()
            data = data[0x248:-0x8A]
            nbVal = int(len(data)/49)
            for k in range(nbVal):
                #print(data[k*49+26:k*49+30], " | ",data[k*49+30:k*49+34])
                coord.write(str(int.from_bytes(data[k*49+26:k*49+30],"little")/10000000) + ";" + str(int.from_bytes(data[k*49+30:k*49+34],"little")/10000000)+"\n")
                #print("-".join(( hex(b)[2:] if (b>15) else '0'+hex(b)[2:]) for b in data[k*49+26:k*49+30]))

parse_GPS(sys.argv[1])

